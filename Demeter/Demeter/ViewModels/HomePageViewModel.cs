﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;

namespace Demeter.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        public HomePageViewModel(INavigationService navigationService) 
            : base(navigationService)
        {
        }
    }
}

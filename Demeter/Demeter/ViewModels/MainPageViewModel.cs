﻿using Demeter.Services;
using Demeter.Views;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Demeter.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public ICommand NavigateCommand { get; }
        public ICommand LogoutCommand { get; }

        public MainPageViewModel(INavigationService navigationService, IAuthenticationService authenticationService)
            : base(navigationService)
        {
            Title = "Main Page";
            LogoutCommand = new DelegateCommand(authenticationService.Logout);
            NavigateCommand = new DelegateCommand<Type>(Navigate);
        }

        private async void Navigate(Type type)
        {
            await NavigationService.NavigateAsync($"/{nameof(MainPage)}/{nameof(NavigationPage)}/{type.Name}");
        }
    }
}

﻿using Demeter.Services;
using Demeter.Views;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Demeter.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IPageDialogService dialogService;

        public ICommand LoginCommand { get; }

        private string _UserName;
        public string UserName
        {
            get => _UserName;
            set => SetProperty(ref _UserName, value);
        }

        private string _Password;
        public string Password
        {
            get => _Password;
            set => SetProperty(ref _Password, value);
        }

        public LoginPageViewModel(INavigationService navigationService, IAuthenticationService authenticationService, IPageDialogService dialogService) 
            : base(navigationService)
        {
            this.authenticationService = authenticationService;
            this.dialogService = dialogService;
            Title = "Login";
            LoginCommand = new DelegateCommand(Login, CanLogin).ObservesProperty(() => UserName).ObservesProperty(() => Password);
        }

        private async void Login()
        {
            if (authenticationService.Login(UserName, Password))
            {
                await NavigationService.NavigateAsync($"{nameof(MainPage)}/{nameof(NavigationPage)}/{nameof(HomePage)}");
            }
            else
            {
                await dialogService.DisplayAlertAsync("Fehlgeschlagen", "Login ist fehlgeschlagen!", "OK");
            }
        }

        private bool CanLogin()
        {
            return !(string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;

namespace Demeter.ViewModels
{
    public class ArticlesPageViewModel : ViewModelBase
    {
        public ArticlesPageViewModel(INavigationService navigationService) 
            : base(navigationService)
        {
        }
    }
}

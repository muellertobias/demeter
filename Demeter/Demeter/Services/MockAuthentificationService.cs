﻿using Demeter.Views;
using Prism.Navigation;

namespace Demeter.Services
{
    public class MockAuthentificationService : IAuthenticationService
    {
        INavigationService _navigationService { get; }

        public MockAuthentificationService(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public bool Login(string username, string password)
        {
            return true;
        }

        public void Logout()
        {
            _navigationService.NavigateAsync($"/{nameof(LoginPage)}");
        }
    }
}
